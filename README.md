# Git Series

Output events and metrics from your Git repositories and store in InfluxDB.

```console
gitseries --directory ~/Code/src/github.com/torvalds/linux analyse | influx write
```
