use clap::Clap;
use git2::Repository;

use crate::git;
use crate::output::influxdb::commit;

#[derive(Clap)]
pub struct Analyse {}

pub fn analyse(directory: String, _args: Analyse) {
    log::debug!("Checking if {} is a valid Git clone", directory);
    let repository: Repository = match Repository::open(&directory) {
        Ok(repository) => repository,
        Err(_) => {
            log::error!("Directory {} is not a valid Git repository", directory);
            return;
        }
    };

    log::debug!("Attempting to parse repository metadata from origin remote");
    let metadata = git::get_repository_metadata(&repository);

    log::debug!("Fetching object database");
    let object_database = repository.odb().unwrap();

    match object_database.foreach(|&object_id| {
        log::debug!("Parsing object ID {}", object_id);
        let object = repository.find_object(object_id, None).expect("Nope");

        use git2::ObjectType::*;
        match object.kind() {
            Some(Blob) => {
                log::debug!("Unhandled object found in Git repository: blob");
            }
            Some(Commit) => {
                log::debug!("Object ({}) is a commit", object_id);

                let c = object.as_commit().unwrap();
                log::info!(
                    "Attempting to parse commit with summary: {}",
                    c.summary().unwrap()
                );

                commit(&repository, &metadata, &c);
            }
            Some(Tag) => {
                log::debug!("Unhandled object found in Git repository: tag");
            }
            Some(Tree) => {
                log::debug!("Unhandled object found in Git repository: tree");
            }
            Some(Any) => {
                log::debug!(
                    "Unhandled object found in Git repository: {}",
                    object.kind().unwrap()
                );
            }
            None => {
                log::debug!("Unknown object found in Git repository");
            }
        }

        return true;
    }) {
        Ok(_) => (),
        Err(error) => {
            log::error!("There was a problem walking the object database: {}", error);
            return;
        }
    }
}
