use git2::{Remote, Repository};
use regex::{Captures, Regex};

pub struct RepositoryMetadata {
    pub name: String,
    pub owner: String,
    pub host: String,
}

pub fn get_repository_metadata(repository: &Repository) -> RepositoryMetadata {
    let remote: Remote = match repository.find_remote("origin") {
        Ok(r) => r,
        Err(_) => panic!("Git repository does not have an 'origin' remote configured."),
    };

    let url = remote.url().unwrap();
    // This regex will allow us to convert the following Git Url formats
    // to our standard format:
    //
    //  - git@git.com:owner/name
    //  - https://git.com/owner/name
    //  - ssh://git@git.com/owner/name
    let re: Regex = match Regex::new(
        "^(?:\\S+://)?(?:\\S+@)?(?P<host>\\S+?)[:/](?P<owner>\\S+?)[/](?P<repository>\\S+)$",
    ) {
        Ok(re) => re,
        _ => unreachable!(),
    };

    let captures: Captures = match re.captures(url) {
        Some(c) => c,
        None => panic!("Couldn't not parse origin url ({}) into metadata.", url),
    };

    return RepositoryMetadata {
        name: String::from(captures.name("repository").unwrap().as_str()),
        owner: String::from(captures.name("owner").unwrap().as_str()),
        host: String::from(captures.name("host").unwrap().as_str()),
    };
}
