use clap::Clap;

mod commands;
use commands::{Analyse, analyse};

mod git;
mod output;

/// gitseries can analyse your Git reflog for events that can be pushed to InfluxDB
#[derive(Clap)]
pub struct Opts {
    /// The directory where the Git repository is located
    #[clap(long, default_value=".")]
    directory: String,

    #[clap(subcommand)]
    commands: Commands,
}

#[derive(Clap)]
enum Commands {
    Analyse(Analyse),
}

fn main() {
    env_logger::init_from_env(env_logger::Env::default().default_filter_or("warn"));
    
    let opts: Opts = Opts::parse();
    
    match opts.commands {
        Commands::Analyse(args) => analyse(opts.directory, args),
    }
}
