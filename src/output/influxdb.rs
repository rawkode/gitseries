use crate::git::RepositoryMetadata;
use git2::{Commit, Diff, DiffOptions, DiffStats, Repository, Tree};

pub fn commit(repository: &Repository, metadata: &RepositoryMetadata, commit: &Commit) {
    let mut diff_options = DiffOptions::new();

    let current_tree: Tree = match commit.tree() {
        Ok(t) => t,
        Err(_) => {
            log::error!("Failed to get the tree for commit with ID {}", commit.id());
            return;
        }
    };

    let parent_tree: Option<Tree> = match commit.parent(0) {
        Ok(parent_commit) => match parent_commit.tree() {
            Ok(t) => Some(t),
            Err(_) => None,
        },
        Err(_) => None,
    };

    let diff: Diff = match repository.diff_tree_to_tree(
        parent_tree.as_ref(),
        Some(&current_tree),
        Some(&mut diff_options),
    ) {
        Ok(d) => d,
        Err(_) => {
            log::error!("Failed to calculate diff for commit {}", commit.id());
            return;
        }
    };

    let stats: DiffStats = match diff.stats() {
        Ok(s) => s,
        Err(_) => {
            log::error!("Failed to calculate diff stats for commit {}", commit.id());
            return;
        }
    };

    println!(
        r#"commit,host={},owner={},repository={},author={} commit={},insertions={},deletions={},message="{}" {}"#,
        metadata.host,
        metadata.owner,
        metadata.name,
        commit.author().email().unwrap(),
        commit.id(),
        stats.insertions(),
        stats.deletions(),
        commit
            .summary()
            .unwrap()
            .replace("\n", "\\n")
            .replace("\"", "\\\""),
        commit.time().seconds(),
    );

    return;
}
